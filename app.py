from urllib import response
import telebot
import requests
import json


bot = telebot.TeleBot("6706407624:AAGaz10954esXgyxgsCjFdMWiaK5IXRWubw")
API = '847eab1e5afc765381e873c349a07691'


@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id, 'Привет, рад тебя видеть! Напиши название города.')


@bot.message_handler(content_types=['text'])
def get_weather(message):
    city = message.text.strip().lower()
    res = requests.get(f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={API}&units=metric')
    if res.status_code == 200:
        data = json.loads(res.text)
        temp = data["main"]["temp"]
        feels_like = data["main"]["feels_like"]
        temp_min = data["main"]["temp_min"]
        temp_max = data["main"]["temp_max"]
        pressure = data["main"]["pressure"]
        humidity = data["main"]["humidity"]
        bot.reply_to(message, f'Сейчас в городе {city} {temp} *C\nОщущается как: {feels_like} *C\nМинимальная температура за день: {temp_min} *C\nМаксимальная температура за день: {temp_max} *C\nДавление: {pressure} мм.рт.ст\nВлажность: {humidity}%')
        bot.send_sticker(message.chat.id, "CAACAgIAAxkBAAEK0axlYtyYnhVoiLZEqe0z55t5M0ITeQAC-BQAAgiJ8Et_5nbmeLkiiDME")
    else:
        bot.reply_to(message, f'Город указан не верно.')


bot.polling(none_stop=True)